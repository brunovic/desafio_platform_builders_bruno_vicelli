package br.com.builders.treinamento.dto.request;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;

@Data
@Getter
@Setter
@NoArgsConstructor
@ToString
public class CustomerRequest implements Serializable {

	private String id;

	@NotNull(message = "The crmId value cannot be null.")
	private String crmId;

	@NotNull(message = "The baseUrl value cannot be null.")
	@Pattern(regexp = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", message = "Invalid URL format.")
	private String baseUrl;

	@NotNull(message = "The name value cannot be null.")
	private String name;

	@NotNull(message = "The email value cannot be null.")
	@Email(message = "Invalid email format.")
	private String login;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public CustomerRequest(String id, String crmId, String baseUrl, String name, String login) {
		this.id = id;
		this.crmId = crmId;
		this.baseUrl = baseUrl;
		this.name = name;
		this.login = login;
	}

}
