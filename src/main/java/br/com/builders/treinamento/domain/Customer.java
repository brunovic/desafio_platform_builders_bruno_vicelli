package br.com.builders.treinamento.domain;

import javax.persistence.Id;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bruno_vicelli_customer")
@Data
@Builder
@ToString
public class Customer {

	@Id
	private String id;
	private String crmId;
	private String baseUrl;
	private String name;
	private String login;

	public Customer() {
	}

	public Customer(String id, String crmId, String baseUrl, String name, String login) {
		this.id = id;
		this.crmId = crmId;
		this.baseUrl = baseUrl;
		this.name = name;
		this.login = login;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
