package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.repository.CustomerRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping(value = "/api/customers")
public class CustomerResource {

	@Autowired
	private CustomerRepository customerRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private static Logger LOGGER = LoggerFactory.getLogger(CustomerResource.class);

	@ApiOperation(value = "List all customers", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Search results matching criteria") })
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CustomerResponse>> listCustomers() {
		LOGGER.info("List all customers");

		List<Customer> customers = customerRepository.findAll();
		List<CustomerResponse> customersList = new ArrayList<>();

		if (Objects.nonNull(customers)) {
			customers.forEach(ct -> customersList.add(modelMapper.map(ct, CustomerResponse.class)));
		}

		LOGGER.info("List all customers occurred successfully, customersList-> " + customersList);
		return ResponseEntity.ok(customersList);
	}

	@ApiOperation(value = "Creates new customer", httpMethod = "POST")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Item created"),
			@ApiResponse(code = 400, message = "Invalid input, object invalid"),
			@ApiResponse(code = 409, message = " An existing item(login) already exists") })
	@ResponseStatus(value = HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createCustomer(@RequestBody @Valid CustomerRequest customerRequest, Errors errors) {

		Customer customer = customerRepository.findByLogin(customerRequest.getLogin());

		if (Objects.nonNull(customer)) {
			LOGGER.error("The customer with login " + customerRequest.getLogin() + " already exists.");
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}

		if (errors.hasErrors()) {
			LOGGER.error("There was an error during Customer creation. Invalid input, object invalid: " + errors
					.getFieldErrors());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}

		Customer customerDB = customerRepository.save(modelMapper.map(customerRequest, Customer.class));

		LOGGER.info("Customer created successfully" + customerDB.toString());

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(customerDB.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	@ApiOperation(value = "List all data about a customer", httpMethod = "GET")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "A single customer"),
			@ApiResponse(code = 404, message = "Customer with this ID not found") })
	@RequestMapping(method = RequestMethod.GET, value = "/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getCustomer(@PathVariable String customerId) throws NotFoundException {
		LOGGER.info("List all data about a customer with ID: " + customerId + "");
		Customer customer = customerRepository.findOne(customerId);

		if (Objects.isNull(customer)) {
			LOGGER.error("Customer with id: " + customerId + " not found!");
			throw new NotFoundException("Customer not found");
		}

		CustomerResponse customerResponse = modelMapper.map(customer, CustomerResponse.class);
		LOGGER.info("List all data about a customer: " + customerResponse.toString());

		return ResponseEntity.ok(customerResponse);
	}

	@ApiOperation(value = "Replaces a customer", httpMethod = "PUT")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item replaced"),
			@ApiResponse(code = 400, message = "Invalid input, object invalid"),
			@ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found") })
	@RequestMapping(method = RequestMethod.PUT, value = "/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity replaceCustomer(@PathVariable String customerId,
			@RequestBody @Valid CustomerRequest customerRequest, Errors errors)
			throws NotFoundException, BadRequestAPIException {

		Customer customer = customerRepository.findOne(customerId);

		if (Objects.isNull(customer)) {
			LOGGER.error("The customer (or any of supplied IDs) is not found");
			throw new NotFoundException("The customer (or any of supplied IDs) is not found");
		}

		if (errors.hasErrors()) {
			LOGGER.error("Invalid input, object invalid: " + errors.getFieldErrors());
			throw new BadRequestAPIException("Invalid input, object invalid");
		}

		Customer customerReplaced = customerRepository.save(modelMapper.map(customerRequest, Customer.class));
		LOGGER.info("Item replaced:" + customerReplaced.toString());

		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Modifies a customer", httpMethod = "PATCH")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item modified"),
			@ApiResponse(code = 400, message = "Invalid input, object invalid"),
			@ApiResponse(code = 404, message = "The customer (or any of supplied IDs) is not found") })
	@RequestMapping(method = RequestMethod.PATCH, value = "/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity modifyCustomer(@RequestBody CustomerRequest customerRequest, Errors errors,
			@PathVariable final String customerId) throws NotFoundException, BadRequestAPIException {

		Customer customer = customerRepository.findOne(customerId);

		if (Objects.isNull(customer)) {
			LOGGER.error("The customer is not found");
			throw new NotFoundException("The customer is not found");
		}

		if (errors.hasErrors()) {
			LOGGER.error("Invalid input, object invalid: " + errors.getFieldErrors());
			throw new BadRequestAPIException("Invalid input, object invalid");
		}

		Customer customerReplaced = customerRepository.save(modelMapper.map(customerRequest, Customer.class));
		LOGGER.info("Item replaced:" + customerReplaced.toString());

		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Deletes a customer", httpMethod = "DELETE")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Item deleted"),
			@ApiResponse(code = 404, message = "TThe customer is not found") })
	@RequestMapping(method = RequestMethod.DELETE, value = "/{customerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deleteCustomer(@PathVariable String customerId) throws NotFoundException {
		Customer customer = customerRepository.findOne(customerId);

		if (Objects.isNull(customer)) {
			LOGGER.error("The customer is not found");
			throw new NotFoundException("The customer is not found");
		}

		customerRepository.delete(customer);
		LOGGER.info("Deletes a customer with id: " + customerId);

		return ResponseEntity.ok().build();
	}
}
