package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.TreinamentoApplication;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.utils.JsonUtil;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TreinamentoApplication.class)
public class CustomerResourceTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc restMvc;

	@Autowired
	private CustomerRepository customerRepository;

	private static final String ID = "553fa88c-4511-445c-b33a-ddff58d76886";
	private static final String CRMID = "C645235";
	private static final String BASE_URL = "http://www.platformbuilders.com.br";
	private static final String NAME = "Platform Builders";
	private static final String LOGIN = "contato@platformbuilders.com.br";

	private static final String API_BASE_PATH = "/api/customers/";

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.restMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void testListCustomers() throws Exception {
		final String secondId = "553fa88c-4511-445c-b33a-ddff58d76888";

		try {
			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			Customer secondCustomer = createCustomerTest();
			secondCustomer.setId(secondId);
			customerRepository.save(secondCustomer);

			List<Customer> customers = customerRepository.findAll();

			this.restMvc.perform(get(API_BASE_PATH)).andExpect(status().isOk()).andExpect(
					jsonPath("$", hasSize(customers.size()))).andExpect(
					jsonPath("$[" + 0 + "].id", is(customers.get(0).getId()))).andExpect(
					jsonPath("$[" + 0 + "].name", is(customers.get(0).getName()))).andExpect(
					jsonPath("$[" + 0 + "].login", is(customers.get(0).getLogin()))).andExpect(
					jsonPath("$[" + 0 + "].baseUrl", is(customers.get(0).getBaseUrl()))).andExpect(
					jsonPath("$[" + 0 + "].crmId", is(customers.get(0).getCrmId()))).andExpect(
					jsonPath("$[" + 1 + "].id", is(customers.get(1).getId()))).andExpect(
					jsonPath("$[" + 1 + "].name", is(customers.get(1).getName()))).andExpect(
					jsonPath("$[" + 1 + "].login", is(customers.get(1).getLogin()))).andExpect(
					jsonPath("$[" + 1 + "].baseUrl", is(customers.get(1).getBaseUrl()))).andExpect(
					jsonPath("$[" + 1 + "].crmId", is(customers.get(1).getCrmId())));

		} finally {
			customerRepository.delete(ID);
			customerRepository.delete(secondId);
		}
	}

	@Test
	public void testCreateCustomer() throws Exception {
		try {

			Customer customer = createCustomerTest();

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(post(API_BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(requestJson))
					.andExpect(status().isCreated());

			Customer customerDB = customerRepository.findById(ID);

			Assert.assertEquals(customer.getBaseUrl(), customerDB.getBaseUrl());
			Assert.assertEquals(customer.getCrmId(), customerDB.getCrmId());
			Assert.assertEquals(customer.getLogin(), customerDB.getLogin());
			Assert.assertEquals(customer.getName(), customerDB.getName());

		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testCreateDuplicatedCustomer() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(post(API_BASE_PATH).contentType(MediaType.APPLICATION_JSON).content(requestJson))
					.andExpect(status().isConflict());

		} finally {
			customerRepository.delete(ID);
		}

	}

	@Test
	public void testCreateInvalidCustomer() throws Exception {
		try {

			this.restMvc.perform(post(API_BASE_PATH).contentType(MediaType.APPLICATION_JSON).content("")).andExpect(
					status().isBadRequest());

		} finally {
			customerRepository.delete(ID);
		}

	}

	@Test
	public void testGetCustomer() throws Exception {

		try {
			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			this.restMvc.perform(get(API_BASE_PATH.concat(ID))).andExpect(status().isOk()).andExpect(
					jsonPath("id", is(ID))).andExpect(jsonPath("name", is(NAME))).andExpect(
					jsonPath("login", is(LOGIN))).andExpect(jsonPath("baseUrl", is(BASE_URL))).andExpect(
					jsonPath("crmId", is(CRMID)));
		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testGetNotFoundCustomer() throws Exception {
		this.restMvc.perform(get(API_BASE_PATH.concat("123"))).andExpect(status().isNotFound());
	}

	@Test
	public void testReplaceCustomer() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			customer.setCrmId(CRMID + "1");
			customer.setBaseUrl(BASE_URL + "/contact");
			customer.setLogin(LOGIN + ".net");
			customer.setName(NAME + "Test");

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(put(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON)
										 .content(requestJson)).andExpect(status().isOk());

			Customer customerDB = customerRepository.findById(ID);
			Assert.assertEquals(BASE_URL.concat("/contact"), customerDB.getBaseUrl());
			Assert.assertEquals(CRMID.concat("1"), customerDB.getCrmId());
			Assert.assertEquals(LOGIN.concat(".net"), customerDB.getLogin());
			Assert.assertEquals(NAME.concat("Test"), customerDB.getName());

		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testReplaceNotFoundCustomer() throws Exception {
		final String idNew = ID + "123";
		try {

			Customer newCustomer = new Customer();
			newCustomer.setId(idNew);
			newCustomer.setCrmId(CRMID + "1");
			newCustomer.setBaseUrl(BASE_URL + "/contact");
			newCustomer.setLogin(LOGIN + ".net");
			newCustomer.setName(NAME + "Test");

			String requestJson = JsonUtil.fromObject(newCustomer);

			this.restMvc.perform(put(API_BASE_PATH.concat("456")).contentType(MediaType.APPLICATION_JSON)
										 .content(requestJson)).andExpect(status().isNotFound());

			Assert.assertNull(customerRepository.findById(idNew));

		} finally {
			customerRepository.delete(idNew);
		}

	}

	@Test
	public void testReplaceInvalidCustomer() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			this.restMvc.perform(put(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON).content(""))
					.andExpect(status().isBadRequest());

			assertNotNull(customerRepository.findById(ID));
		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testPatchBaseURL() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			customer.setBaseUrl(BASE_URL.concat("/contact"));

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(patch(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON)
										 .content(requestJson)).andExpect(status().isOk());

			Customer customerDB = customerRepository.findById(ID);
			Assert.assertEquals(BASE_URL.concat("/contact"), customerDB.getBaseUrl());
			Assert.assertEquals(CRMID, customerDB.getCrmId());
			Assert.assertEquals(LOGIN, customerDB.getLogin());
			Assert.assertEquals(NAME, customerDB.getName());

		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testPatchCRMID() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			customer.setCrmId(CRMID.concat("1"));

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(patch(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON)
										 .content(requestJson)).andExpect(status().isOk());

			Customer customerDB = customerRepository.findById(ID);
			Assert.assertEquals(CRMID.concat("1"), customerDB.getCrmId());
			Assert.assertEquals(BASE_URL, customerDB.getBaseUrl());
			Assert.assertEquals(LOGIN, customerDB.getLogin());
			Assert.assertEquals(NAME, customerDB.getName());

		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testPatchLogin() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			customer.setLogin(LOGIN.concat(".net"));

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(patch(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON)
										 .content(requestJson)).andExpect(status().isOk());

			Customer customerDB = customerRepository.findById(ID);
			Assert.assertEquals(LOGIN.concat(".net"), customerDB.getLogin());
			Assert.assertEquals(BASE_URL, customerDB.getBaseUrl());
			Assert.assertEquals(CRMID, customerDB.getCrmId());
			Assert.assertEquals(NAME, customerDB.getName());

		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testPatchName() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			customer.setName(NAME.concat("Test"));

			String requestJson = JsonUtil.fromObject(customer);

			this.restMvc.perform(patch(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON)
										 .content(requestJson)).andExpect(status().isOk());

			Customer customerDB = customerRepository.findById(ID);
			Assert.assertEquals(NAME.concat("Test"), customerDB.getName());
			Assert.assertEquals(BASE_URL, customerDB.getBaseUrl());
			Assert.assertEquals(CRMID, customerDB.getCrmId());
			Assert.assertEquals(LOGIN, customerDB.getLogin());

		} finally {
			customerRepository.delete(ID);
		}
	}

	@Test
	public void testPatchNotFoundCustomer() throws Exception {
		Customer newCustomer = createCustomerTest();
		newCustomer.setCrmId(CRMID + "1");
		newCustomer.setBaseUrl(BASE_URL + "/contact");
		newCustomer.setLogin(LOGIN + ".net");
		newCustomer.setName(NAME + "Test");
		String requestJson = JsonUtil.fromObject(newCustomer);

		this.restMvc.perform(patch(API_BASE_PATH.concat("123")).contentType(MediaType.APPLICATION_JSON)
									 .content(requestJson)).andExpect(status().isNotFound());

	}

	@Test
	public void testPatchInvalidCustomer() throws Exception {
		try {

			Customer customer = createCustomerTest();
			customerRepository.save(customer);

			this.restMvc.perform(patch(API_BASE_PATH.concat(ID)).contentType(MediaType.APPLICATION_JSON).content(""))
					.andExpect(status().isBadRequest());

		} finally {
			customerRepository.delete(ID);
		}

	}

	@Test
	public void testDeleteCustomer() throws Exception {
		Customer customer = createCustomerTest();
		customerRepository.save(customer);

		this.restMvc.perform(delete(API_BASE_PATH.concat(ID))).andExpect(status().isOk());

		Assert.assertNull(customerRepository.findById(ID));
	}

	@Test
	public void testDeleteCustomerNotFound() throws Exception {
		this.restMvc.perform(delete(API_BASE_PATH.concat("123"))).andExpect(status().isNotFound());
	}

	private Customer createCustomerTest() {

		Customer customer = new Customer();
		customer.setId(ID);
		customer.setCrmId(CRMID);
		customer.setBaseUrl(BASE_URL);
		customer.setName(NAME);
		customer.setLogin(LOGIN);

		return customer;

	}

}
